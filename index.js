
class Student {
    constructor(fname, email, grades) {
        this.fname = fname;
        this.email = email;
        this.students = [];
        if (grades.length === 4) {
            if (grades.every(grade => grade >= 0 && grade <= 100)) {
                this.grades = grades;
            } else {
                this.grades = undefined;
            }
        } else {
            this.grades = undefined;
        }
    }
      addStudent(fname, email, grades) {
        // a Student object will be instantiated before before being pushed to the students array property
        this.students.push(new Student(fname, email, grades));
        return this;
    }
}

class Grade{
    constructor(level){
      
        this.level = level;
        this.sections = [];
        this.totalStudents = 0;
        this.totalHonorStudents = 0;
        this.batchAveGrade = undefined;
        this.batchMaxGrade = undefined;
        this.batchMinGrade = undefined;
    }
    addSection(name, students, honorStudents) {
        // a Student object will be instantiated before before being pushed to the students array property
        this.sections.push(
            {
                "name": name,
                "students": students=[],
                "honorStudents": honorStudents
            }
            );
        return this;
    }

}



const grade1 = new Grade(1);
console.log(`
1. Define a Grade class whose constructor will accept a number argument to serve as its grade level. It will have the following properties:
level initialized to passed in number argument
sections initialized to an empty array
totalStudents initialized to zero
totalHonorStudents initialized to zero
batchAveGrade set to undefined
batchMinGrade set to undefined
batchMaxGrade set to undefined
`);



console.log(grade1);
//console.log(typeof level);



console.log(`
2.Define an addSection() method that will take in a string argument to instantiate a new Section object and push it into the sections array property.
`);

//grade1.addSection('section1A')

console.log(grade1.addSection('section1A'));

console.log(`
3.Write a statement that will add a student to section1A. The student is named John, with email john@mail.com, and grades of 89, 84, 78, and 88.
`);

const section1A = grade1.sections.find(section => section.name === "section1A");

//console.log(section1A.addStudent('John', 'john@mail.com', [89, 84, 78, 88]));


console.log(grade1.sections.find(e => e.name === "section1A").addStudent('John', 'john@mail.com', [89, 84, 78, 88]));
